const express = require("express");
const router = express.Router();

const accountManager = require("../account/acount_manager");

router.post("/register", (req, res, next) => {
  accountManager.createAccount(req, function(err, objData) {
    if (err) {
      res.status(400).json({
        message: ' Account Creation failed'
      });
      // res.send(objData.result);
      return;
    }
    res.status(201).json({
      message: ' Account Created!'
    });
		// res.send(objData.result);
  });
});

module.exports = router;

