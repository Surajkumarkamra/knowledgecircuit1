/**
* HTTP/REST Response Code
*/
exports.httpResponseCode = {
	"OK": 200,
	"CREATED": 201,
	"ACCEPTED": 202,
	"NO_CONTENT": 204,
	"MOVED_PERMANENTLY": 301,
	"NOT_MODIFIED": 304,
	"TEMPORARY_REDIRECT": 307,
	"BAD_REQUEST": 400,
	"UNAUTHORIZED": 401,
	"FORBIDDEN": 403,
	"NOT_FOUND": 404,
	"METHOD_NOT_ALLOWED": 405,
	"REQUEST TIMEOUT": 408,
	"CONFLICT": 409,
	"PRECONDITION_FAILED": 412,
	"UNSUPPORTED_MEDIA_TYPE": 415,
	"UNPROCESSABLE_ENTITY": 422,
	"TOO_MANY_REQUESTS": 429,
	"INTERNAL_SERVER_ERROR": 500
};

exports.userType = {
	"LEARNER": "learner",
	"EDUCATOR": "educator",
	"ADMIN": "admin"
};

exports.accountStatus = {
	"INACTIVE": "inactive",
	"DEACTIVE": "deactive",
	"ACTIVE": "active"
};

exports.objectIdPatternValidator = new RegExp("^[0-9a-fA-F]{24}$");
exports.mobileNumberValidator = new RegExp(/^([0|\+[0-9]{2})?([6-9][0-9]{10})$/);
exports.emailIdValidator = new RegExp(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/);
exports.sensorTopicPatternValidator = new RegExp(/^(mqtt\/)([A-Fa-f0-9]{24})(\/sensordata)$/);
exports.eventTopicPatternValidator = new RegExp(/^(mqtt\/)([A-Fa-f0-9]{24})(\/event)$/);
exports.configureTopicPatternValidator = new RegExp(/^(mqtt\/)([A-Fa-f0-9]{24})(\/configure)$/);
exports.cmdRequestTopicPatternValidator = new RegExp(/^(mqtt\/)([A-Fa-f0-9]{24})(\/cmdrequest)$/);
exports.cmdResponseTopicPatternValidator = new RegExp(/^(mqtt\/)([A-Fa-f0-9]{24})(\/cmdresponse)$/);
exports.registerTopicPatternValidator = new RegExp(/^(mqtt\/register)$/);
