var jwt = require("jsonwebtoken");
var bcrypt = require("bcrypt");

var constants = require("./constant");

exports.invalidKeys = function(obj, arrayKey) {
	var keys = [];
	var i;

	for(i = 0; i < arrayKey.length; i++ ) {
		if(!obj.hasOwnProperty(arrayKey[i])) {
			keys.push(arrayKey[i]);
		}
	}
	return keys;
};


exports.hashPassword = function(passwordToEncrypt, callback) {
	var saltRounds = 12;

	bcrypt.hash(passwordToEncrypt, saltRounds, function(err, hash) {
		if (err) {
			logger.log(constants.facilityLevel.INIT, constants.severityLevel.ERROR, err);
		}
		return callback(err, hash);
	});
};
