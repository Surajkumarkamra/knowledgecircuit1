const mongoose = require('mongoose');

const user = mongoose.Schema({
  firstname: { type: String, require: true },
  lastname: { type: String, require: true },
  email: { type: String, require: true, unique: true },
  password: { type: String, require: true },
  account_type: { type: String, require: true },
  user_type: { type: String, require: true },
  status : { type: String, require: true },
  user_id: {  type: String, require: true, unique: true },
  account_id: { type: String, require: true, unique: true },
});

module.exports =  mongoose.model('user', user);
