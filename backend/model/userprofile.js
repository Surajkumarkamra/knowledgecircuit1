const mongoose = require('mongoose');

const userProfile = mongoose.Schema({
  personal: {
    firstname: { type: String, require: true },
    lastname: { type: String, require: true },
    user_name: { type: String, require: true },
    phone_no: { type: String, require: true },
    mobile_no: { type: String, require: true },
    gender: { type: String, require: true },
    dob: { type: String, require: true },
    house_no: { type: String, require: true },
    street: { type: String, require: true },
    city: { type: String, require: true },
    state: { type: String, require: true },
    country: { type: String, require: true },
    pincode: { type: String, require: true },
  },
  knowledge: {
    education: { type: Array },
    other_certification: { type: Array},
    goal: { type: String, require: true },
    interest: { type: Array, require: true },
    about_me: { type: String, require: true },
  },
  resources: {
    device: { type: Array, require: true },
    social_network_details: { type: Array, require: true },
  },
  user_id: { type: String, require: true },
  account_id: { type: String, require: true },
  account_id: { type: String, require: true },
  account_type: { type: String, require: true },
  profile_image: { type: String, require: true },
  user_type: { type: String, require: true },
  male_count: { type: Number, require: true },
  female_count: { type: Number, require: true },
});

module.exports =  mongoose.model('userProfile', userProfile);
