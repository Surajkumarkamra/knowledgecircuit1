var uuid = require("node-uuid");
var mongoose = require('mongoose');

var helper = require("../helper/helper");
var constants = require("../helper/constant");
var User = require("../model/user");
var Profile =  require("../model/userprofile");


exports.createAccount = function (req, callback) {
	validateUserDetails(req.body, function(err, data) {
		if(err) {
      return callback(err, data);
    }
    helper.hashPassword(req.body.password, function(err, pwdHash){
      if(err) {
				return callback(err, helper.responseMessage(constants.httpResponseCode.INTERNAL_SERVER_ERROR,
					"Unable to process request. Please try after some time!"));
      }
      req.body.password = pwdHash;
      createUser(req.body, function(err, result) {
        if(err) { return callback(err, result); }
        objUser = req.body;
				objUser.user_id = result.user_id.toString();
				req.user = objUser;
        return callback(err, result);
        // if(constants.emailIdValidator.test(req.body.username)) {
				// 	req.tfa_token = utility.createTFAToken(req.ip);
				// }
      });
    });
  });
};


function validateUserDetails(objUser, callback) {
  var arrayKey, keys;

  arrayKey = ["firstname", "lastname", "email", "password", "account_type", "user_type"];
  keys = helper.invalidKeys(objUser, arrayKey);

  if (keys.length > 0) {
    return callback("error", {
      "status": constants.httpResponseCode.PRECONDITION_FAILED,
      "result": {
        "message": "Key " + keys.join(", ") + " not found."
      }
    });
  }

  if (!(constants.mobileNumberValidator.test(objUser.email)) && !(constants.emailIdValidator.test(objUser.email))) {
    return callback("Invalid user name provided.",  constants.httpResponseCode.BAD_REQUEST);
  }

  User.find({email : objUser.email}, function (err, user) {
    if (user.length > 0) {
      return callback("User already exist.",  constants.httpResponseCode.BAD_REQUEST);
    } else {
      return callback(err, user);
    }
  })
}

function createUser(objUser, callback) {
  const user = new User({
    firstname: objUser.firstname,
    lastname: objUser.lastname,
    email: objUser.email,
    password: objUser.password,
    account_type: objUser.account_type,
    user_type: objUser.user_type,
    status: constants.accountStatus.INACTIVE,
    user_id: 'U' + mongoose.Types.ObjectId(),
    account_id: 'A' + mongoose.Types.ObjectId()
  });
  user.save(function (err, result) {
    const userProfile = new Profile({
      personal: {
        firstname: objUser.firstname,
        lastname: objUser.lastname,
        user_name: constants.emailIdValidator.test(objUser.email) ? objUser.email : "",
        phone_no: "",
        mobile_no: constants.mobileNumberValidator.test(objUser.email) ? objUser.email : "",
        gender: "",
        dob: "",
        house_no: "",
        street: "",
        city: "",
        state: "",
        country: "",
        pincode: "",
      },
      knowledge: {
        education: "",
        other_certification: "",
        goal: "",
        interest: "",
        about_me: ""
      },
      resources: {
        device: "",
        social_network_details: "",
      },
      user_id: result.user_id,
      account_type: objUser.account_type,
      profile_image: "",
      user_type: objUser.account_type,
      male_count: "",
      female_count: "",
    });
    userProfile.save(function (err, profileResult) {
      return callback(err, profileResult);
    });
  });
}

