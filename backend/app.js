const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const accountRoute = require("./routes/route_account");
const app = express();

mongoose.connect("mongodb+srv://kcdb:B04xrssdvaI8hgq5@kc-db-5cdoi.gcp.mongodb.net/kc-database?retryWrites=true&w=majority", { useNewUrlParser: true })
  .then(() => {
    console.log("Conneted to DB");
  })
  .catch(() => {
    console.log("Failed to connect DB");
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept");
  res.setHeader("Access-Control-Allow-Method",
    "GET, POST, PUT, DELETE, OPTIONS");
  next();
});


app.use("/api/account", accountRoute);

module.exports = app;
