import { Component, OnInit, AfterViewChecked, OnDestroy, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'anms-learner',
  templateUrl: './learner.component.html',
  styleUrls: ['./learner.component.scss']
})
export class LearnerComponent implements OnInit, AfterViewChecked, OnDestroy {

  constructor(private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
  }

    /**
   * After view checked.
   */
  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  /**
   * On Destroy component.
   */
  ngOnDestroy() {
  }

}
