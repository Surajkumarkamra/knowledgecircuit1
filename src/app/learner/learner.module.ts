import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { MainScreenModule } from '../main-screen';

import { LearnerComponent } from './learner/learner.component';



const learnerRoutes: Routes = [{
  path: '',
  component: LearnerComponent,
  data: { title: 'Learner' },
}];

@NgModule({
  imports: [
    CommonModule,
    MainScreenModule,
    RouterModule.forChild(learnerRoutes)
  ],
  exports: [RouterModule],
  declarations: [LearnerComponent]
})
export class LearnerModule { }
