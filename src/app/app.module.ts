import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AboutComponent } from './about/about.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { HomepageComponent } from './homepage/homepage.component';

import { MaterialModule } from './material-module';
import { CoreModule } from '@app/core';
import { SettingsModule } from './settings';
import { AppRoutingModule } from './app-routing.module';

import { SlideshowModule } from 'ng-simple-slideshow';

@NgModule({
  imports: [
    // angular
    BrowserAnimationsModule,
    BrowserModule,

    // core & shared
    CoreModule,
    MaterialModule,

    // features
    SettingsModule,

    // app
    AppRoutingModule,

    SlideshowModule
  ],
  declarations: [AppComponent, PageNotFoundComponent, AboutComponent, ContactUsComponent, HomepageComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
