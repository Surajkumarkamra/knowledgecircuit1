import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAccountConfirmComponent } from './user-account-confirm.component';

describe('UserAccountConfirmComponent', () => {
  let component: UserAccountConfirmComponent;
  let fixture: ComponentFixture<UserAccountConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAccountConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAccountConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
