import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MatTableDataSource, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { emailValidator } from '@app/shared';
import { Router } from '@angular/router';
import { AuthService } from '../../services';
import { UserAddEducationComponent } from '../user-add-education/user-add-education.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

const interestList = [
  { interest: 'IAS' },
  { interest: 'IPS' },
  { interest: 'CA' },
  { interest: 'Law' },
  { interest: 'IES' },
  { interest: 'SSB' },
];
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  userProfile;
  newResource = {
    resourceName: '',
    duration: ''
  };
  settings = {
    isAccountType: true,
    isPassword: true,
    isDeleteUser: true,
  };
  personalForm: FormGroup;
  knowledgeForm: FormGroup;
  isAdd = false;
  isEdit = false;
  isGroup = true;
  public max = new Date();

  interestDisplayedColumns: string[] = ['select', 'interest'];
  interestDataSource = new MatTableDataSource<any>(interestList);
  selection = new SelectionModel<any>(true, []);

  socialSource: MatTableDataSource<any>;
  socialDisplayedColumns: string[] = ['plateForm', 'userName'];
  social = [
    { plateForm: 'Gmail', userName: 'abc@xyz.com' },
    { plateForm: 'Yahoo', userName: 'abc@xyz.com' },
    { plateForm: 'Facebook', userName: 'abc@xyz.com' },
    { plateForm: 'Instagram', userName: 'abc@xyz.com' },
  ];

  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['resourceName', 'permanent', 'temporary'];
  assessments = [
    { resourceName: 'JavaScript', duration: 'permanent' },
    { resourceName: 'Java', duration: 'permanent' },
    { resourceName: 'C#', duration: 'permanent' },
    { resourceName: 'Python', duration: 'permanent' }];
  groupType = [
    { value: 'professionals', viewValue: 'Professionals' },
    { value: 'corporate', viewValue: 'Corporate' },
    { value: 'other', viewValue: 'Other' }
  ];
  durations = [
    { value: 'permanent', viewValue: 'Permanent' },
    { value: 'temporary', viewValue: 'Temporary' },
  ];

  constructor(private fb: FormBuilder,
    private router: Router,
    private authSevice: AuthService,
    private httpclient: HttpClient,
    public dialog: MatDialog) {
    this.authSevice.getUserProfile(this.authSevice.getUser().id).subscribe(
      res => console.log(res),
      err => console.log(err),
    );
  }

  ngOnInit() {

    if (!this.authSevice.getUser()['id']) { this.router.navigate(['/login']); }

    this.personalForm = this.fb.group({
      name: ['', Validators.compose([Validators.required, Validators.maxLength(30)])],
      user_name: ['', Validators.compose([Validators.required, emailValidator])],
      phone_no: ['', Validators.required],
      mobile_no: ['', Validators.required],
      gender: ['', Validators.required],
      dob: ['', Validators.required],
      house_no: [''],
      street: [''],
      city: [''],
      state: [''],
      country: [''],
      pincode: [''],
    });

    this.knowledgeForm = this.fb.group({
      goal: ['', Validators.required],
    });
    this.httpclient.get('./assets/user-profile.json').subscribe(data => {
      this.userProfile = data;
      this.setFormData();
    });
  }

  setFormData() {
    this.userProfile['personal']['dob'] = new Date(this.userProfile['personal']['dob']);

    this.personalForm.controls['name'].setValue(this.userProfile['personal']['name']);
    this.personalForm.controls['user_name'].setValue(this.userProfile['personal']['user_name']);
    this.personalForm.controls['mobile_no'].setValue(this.userProfile['personal']['mobile_no']);
    this.personalForm.controls['phone_no'].setValue(this.userProfile['personal']['phone_no']);
    this.personalForm.controls['gender'].setValue(this.userProfile['personal']['gender']);
    this.personalForm.controls['dob'].setValue(this.userProfile['personal']['dob']);
    this.personalForm.controls['house_no'].setValue(this.userProfile['personal']['house_no']);
    this.personalForm.controls['street'].setValue(this.userProfile['personal']['street']);
    this.personalForm.controls['city'].setValue(this.userProfile['personal']['city']);
    this.personalForm.controls['state'].setValue(this.userProfile['personal']['name']);
    this.personalForm.controls['country'].setValue(this.userProfile['personal']['country']);
    this.personalForm.controls['pincode'].setValue(this.userProfile['personal']['pincode']);

    this.knowledgeForm.controls['goal'].setValue(this.userProfile['knowledge']['goal']);

    this.userProfile['knowledge']['interest'].forEach(item => {
      const selectedItem = this.interestDataSource.data.find(item1 => item1.interest === item);
      this.selection.select(selectedItem);
    });
    this.interestDataSource.data.forEach((item, index) => {
      if (item.isSelected) {
        const tempItem = item;
        this.interestDataSource.data.splice(index, 1);
        this.interestDataSource.data.unshift(tempItem);
      }
    });
    this.dataSource = new MatTableDataSource<any>(this.assessments);
    this.socialSource = new MatTableDataSource<any>(this.social);
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.interestDataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.interestDataSource.data.forEach(row => this.selection.select(row));
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  setResourceData(row, value) {
    this.assessments[this.assessments.indexOf(row)] = {
      ...row,
      duration: value
    };
  }

  addRow() {
    this.dataSource.data.push({
      resourceName: this.newResource.resourceName,
      duration: this.newResource.duration
    });
    this.dataSource.filter = '';
    this.isAdd = !this.isAdd;
  }

  onSubmit() {
  }

  openAddEducationDailog() {
    const dialogRef = this.dialog.open(UserAddEducationComponent, {
      width: '100%',
      height: '100%',
      minHeight: '100%',
      minWidth: '100%'

    });
  }

  backClicked() {
    this.router.navigate(['/dashboard']);
  }

}
