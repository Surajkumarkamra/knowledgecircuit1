import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAddEducationComponent } from './user-add-education.component';

describe('UserAddEducationComponent', () => {
  let component: UserAddEducationComponent;
  let fixture: ComponentFixture<UserAddEducationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAddEducationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAddEducationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
