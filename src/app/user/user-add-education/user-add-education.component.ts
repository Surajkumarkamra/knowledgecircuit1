import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-user-add-education',
  templateUrl: './user-add-education.component.html',
  styleUrls: ['./user-add-education.component.scss']
})
export class UserAddEducationComponent implements OnInit {

  year = [];
  range = [];

  addEducationForm: FormGroup;

  loading = false;
  submitted = false;
  yearErr = false;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {

    this.addEducationForm = this.fb.group({
      school: ['', Validators.required],
      degree: ['', Validators.required],
      study: ['', Validators.required],
      syear: ['', Validators.required],
      eyear: ['', Validators.required],
      grade: ['', Validators.required],
      act: ['', Validators.required],
    });

    this.selectYear();
  }

  selectYear() {
    const year = new Date().getFullYear();
    this.range.push(year);
    for (let i = 1; i < 50; i++) {
      this.range.push(year - i);
    }
    // console.log(this.range);
  }

  get f() { return this.addEducationForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.addEducationForm.invalid) {
      return;
    }

    alert('Details saved successfully');
  }

  compareTwoDates() {

    if (parseInt(this.addEducationForm.controls['eyear'].value, 10) <= this.addEducationForm.controls['syear'].value) {
      this.yearErr = true;
    } else { this.yearErr = false; }
    console.log(this.yearErr);
    console.log(this.f);
  }
}
