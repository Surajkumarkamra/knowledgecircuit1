import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserChangeAccountTypeComponent } from './user-change-account-type.component';

describe('UserChangeAccountTypeComponent', () => {
  let component: UserChangeAccountTypeComponent;
  let fixture: ComponentFixture<UserChangeAccountTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserChangeAccountTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserChangeAccountTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
