import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserAccountConfirmComponent } from './user-account-confirm/user-account-confirm.component';
import { UserForgotPasswordComponent } from './user-forgot-password/user-forgot-password.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserSignUpComponent } from './user-sign-up/user-sign-up.component';
import { UserAddEducationComponent } from './user-add-education/user-add-education.component';
import { UserHomepageComponent } from './user-homepage/user-homepage.component';
import { UserChangePasswordComponent } from './user-change-password/user-change-password.component';
import { UserResetPasswordComponent } from './user-reset-password/user-reset-password.component';
import { MaterialModule } from '@app/material-module';
import { SharedModule } from '@app/shared/shared.module';

import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { SlideshowModule } from 'ng-simple-slideshow';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { UserChangeAccountTypeComponent } from './user-change-account-type/user-change-account-type.component';

const routes: Routes = [
  { path: '', redirectTo: 'homepage', pathMatch: 'full' },
  { path: 'login', component: UserLoginComponent, data: { title: 'Login' } },
  { path: 'sign-up', component: UserSignUpComponent, data: { title: 'Sign Up' } },
  { path: 'homepage', component: UserHomepageComponent, data: { title: 'Homepage' } },
  { path: 'forget-password', component: UserForgotPasswordComponent, data: { title: 'Forget Password' } },
  { path: 'change-password', component: UserChangePasswordComponent, data: { title: 'Change Password' } },
  { path: 'confirm', component: UserAccountConfirmComponent, data: { title: 'Confirm account' } },
  { path: 'profile', component: UserProfileComponent, data: { title: 'Profile' } }
];

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    HttpClientModule,
    RouterModule.forChild(routes),
    SlideshowModule,
    ReactiveFormsModule,
    SharedModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    FormsModule
  ],
  declarations: [
    UserAccountConfirmComponent,
    UserForgotPasswordComponent,
    UserLoginComponent,
    UserSignUpComponent,
    UserAddEducationComponent,
    UserHomepageComponent,
    UserProfileComponent,
    UserChangePasswordComponent,
    UserResetPasswordComponent,
    UserChangeAccountTypeComponent],
  providers: [],
  entryComponents: [UserAddEducationComponent]
})
export class UserModule { }
