import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services';
import { ActionAuthLogin } from '@app/core';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.scss']
})
export class UserLoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  error = '';
  isClicked = false;


  constructor(public authService: AuthService,
    private router: Router,
    private store: Store<any>,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    localStorage.clear();
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });

  }

  get f() { return this.loginForm.controls; }

  onSubmit() {

    this.loading = true;
    this.authService.login(this.loginForm.value.username, this.loginForm.value.password).subscribe(
      res0 => {
        this.loading = false;
        sessionStorage.setItem('USER', res0);
        this.authService.setUser(res0);
        this.store.dispatch(new ActionAuthLogin());
        this.router.navigate(['/dashboard']);
      },
      err => {
      alert('Failed to Login. Try Later!');
      this.loading = false; }
    );
  }

  showUserRoleDialog() {
    this.router.navigate(['/sign-up']);
  }
}
