import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { matchOtherValidator, emailValidator} from '../../shared/validators';
@Component({
  selector: 'app-user-sign-up',
  templateUrl: './user-sign-up.component.html',
  styleUrls: ['./user-sign-up.component.scss']
})
export class UserSignUpComponent implements OnInit {

  signUpForm: FormGroup;
  passwordError = '';
  namePlaceHolder = {
    'firstname': 'First name',
    'lastname': 'Last name'
  };
  hide = true;
  reHide = true;
  loading = false;

  constructor(private authService: AuthService,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.signUpForm = this.fb.group({
      userType: ['learner'],
      accountType: ['single'],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, emailValidator])],
      password: ['', Validators.required],
      rePassword: ['', Validators.compose([Validators.required, matchOtherValidator('password')])],
      terms: ['', Validators.required]
    });
    this.accountTypeValue();
  }

  accountTypeValue() {
    this.namePlaceHolder['firstname'] = this.signUpForm.value.accountType === 'group' ? 'Leader\'s first mame' : 'First name';
    this.namePlaceHolder['lastname'] = this.signUpForm.value.accountType === 'group' ? 'Leader\'s last mame' : 'Last name';
  }

  validateCheckBox(event) {
    this.signUpForm.patchValue({ terms: event.checked });
    this.signUpForm.controls['terms'].updateValueAndValidity();
  }

  isPasswordValid() {
    if (!this.signUpForm.value.password) {
      return false;
    }
    const containSpecialCharacter = /[~`!#@$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/g.test(this.signUpForm.value.password);
    const hasUppercase = !(this.signUpForm.value.password === this.signUpForm.value.password.toLowerCase());
    const hasLowercase = !(this.signUpForm.value.password === this.signUpForm.value.password.toUpperCase());
    const isAtLeast6 = this.signUpForm.value.password.length >= 6;
    const hasNumber = /\d/.test(this.signUpForm.value.password);
    let error = 'Password must contain ';

    if (containSpecialCharacter && hasUppercase && hasLowercase && isAtLeast6 && hasNumber) {
      return true;
    } else {
      if (!containSpecialCharacter) { error = error + '1 special character, '; }
      if (!hasUppercase) { error = error + '1 upper case, '; }
      if (!hasLowercase) { error = error + '1 lower case, '; }
      if (!isAtLeast6) { error = error + '6 characters long, '; }
      if (!hasNumber) { error = error + '1 number'; }
      this.passwordError = error;
      if (this.passwordError.charAt(this.passwordError.length - 2) === ',') {
        this.passwordError = this.passwordError.slice(0, this.passwordError.length - 2);
      }
      return false;
    }
  }

  onSubmit() {

    this.loading = true;
    const body = {
      user_type: this.signUpForm.value.userType,
      account_type: this.signUpForm.value.accountType,
      firstname: this.signUpForm.value.firstname,
      lastname: this.signUpForm.value.lastname,
      email: this.signUpForm.value.email,
      password: this.signUpForm.value.password
      };

    this.authService.createUser(body).subscribe(
      res => {
        if (res['message']) {
          alert(res['message']);
          this.loading = false;
        }
      },
      err => {
        alert('Fail to create Account! Try Later');
        this.loading = false;
      }
    );
  }

}
