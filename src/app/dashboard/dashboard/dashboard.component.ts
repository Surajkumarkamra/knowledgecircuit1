import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { AuthService } from '../../services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs/';
import { routerTransition } from '../../core';

@Component({
  selector: 'anms-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [routerTransition]
})
export class DashboardComponent implements OnInit, OnDestroy {

  user;
  profile_completed = 50;
  private unsubscribe$: Subject<void> = new Subject<void>();

  constructor(private router: Router,
    private authSevice: AuthService) { }

  ngOnInit() {
    if (!this.authSevice.getUser().id) { this.router.navigate(['/login']); }

    this.user = this.authSevice.getUser();
    if (this.authSevice.getUser().id) {
      this.router.navigate(['/dashboard/' + this.authSevice.getUser().user_type]);
    }
  }

  ngOnDestroy(): void {
    // this.unsubscribe$.next();
    // this.unsubscribe$.complete();
  }

}
