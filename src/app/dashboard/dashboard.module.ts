import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { MaterialModule } from '../material-module';
import { StarRatingModule } from 'angular-star-rating';
import { NgCircleProgressModule } from 'ng-circle-progress';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    StarRatingModule.forRoot(),
    NgCircleProgressModule.forRoot({
      'backgroundPadding': 7,
      'radius': 60,
      'space': -2,
      'outerStrokeWidth': 2,
      'outerStrokeColor': '#808080',
      'innerStrokeColor': '#e7e8ea',
      'innerStrokeWidth': 2,
      'titleFontSize': '12',
      'subtitleFontSize': '20',
      'animateTitle': false,
      'animationDuration': 1000,
      'showUnits': false,
      'showTitle': false,
      'clockwise': false,
      'title': [
        'Reort',
        'card'
      ]
    }),
    DashboardRoutingModule
  ],
  exports: [],
  declarations: [DashboardComponent]
})
export class DashboardModule { }
