import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services';

@Component({
  selector: 'app-main-screen-user',
  templateUrl: './main-screen-user.component.html',
  styleUrls: ['./main-screen-user.component.scss']
})
export class MainScreenUserComponent implements OnInit {

  label = '';
  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.label = this.setLable();
  }

  setLable() {
    const label = this.authService.getUser().user_type;
    return label.charAt(0).toUpperCase() + label.substr(1);
  }

}
