import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainScreenUserComponent } from './main-screen-user.component';

describe('MainScreenUserComponent', () => {
  let component: MainScreenUserComponent;
  let fixture: ComponentFixture<MainScreenUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainScreenUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainScreenUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
