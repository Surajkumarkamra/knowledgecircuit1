import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-question-screen',
  templateUrl: './question-screen.component.html',
  styleUrls: ['./question-screen.component.scss']
})
export class QuestionScreenComponent implements OnInit {

  question = '';
  selected = '';
  constructor() { }

  ngOnInit() {
  }

}
