import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainScreenUserComponent } from './main-screen-user/main-screen-user.component';
import { QuestionScreenComponent } from './question-screen/question-screen.component';
import { FavouriteScreenComponent } from './favourite-screen/favourite-screen.component';
import { AnnouncementScreenComponent } from './announcement-screen/announcement-screen.component';

import { MaterialModule } from '../material-module';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  declarations: [
    MainScreenUserComponent,
    QuestionScreenComponent,
    FavouriteScreenComponent,
    AnnouncementScreenComponent
  ],
  exports: [
    MainScreenUserComponent,
    QuestionScreenComponent,
    FavouriteScreenComponent,
    AnnouncementScreenComponent
  ]
})
export class MainScreenModule { }
