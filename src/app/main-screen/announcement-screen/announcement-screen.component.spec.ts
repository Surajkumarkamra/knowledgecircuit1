import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnouncementScreenComponent } from './announcement-screen.component';

describe('AnnouncementScreenComponent', () => {
  let component: AnnouncementScreenComponent;
  let fixture: ComponentFixture<AnnouncementScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnouncementScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnouncementScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
