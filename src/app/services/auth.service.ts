import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/';
import { map } from 'rxjs/operators';
import { User } from '../_helper/user';
import { LocalStorageService } from '../core';
import { Constants } from '../shared';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // serverUrl = 'http://192.168.0.103:3000';
   serverUrl = 'http://knowledgecircuit.com:1337';

  constructor(
    private httpClient: HttpClient,
    private localStorageService: LocalStorageService) {
  }

  login(username: string, password: string) {
    const body = {
      email: username,
      password: password
    };
    const url = this.serverUrl + '/login';
    return this.httpClient.post<any>(url, body);
  }

  createUser(body): Observable<HttpResponse<any>> {
    const url = this.serverUrl + '/api/account/register';
    return this.httpClient.post<any>(url, body);
  }

  logout() {
    const url = this.serverUrl + '/logout';
    return this.httpClient.get<any>(url);
  }

    /**
   * Get User details from local storage.
   */
  public getUser(): any {
    const data = this.localStorageService.getItem('USER');
    if (data) {
      return JSON.parse(atob(data));
    } else { return {}; }
  }

  /**
   * Set User details to local storage.
   */
  public setUser(user: any): void {
    this.localStorageService.setItem('USER', btoa(JSON.stringify(user)));
  }

  public getUserProfile(userId): Observable<HttpResponse<any>> {
    const url = this.serverUrl + '/getUserDetails';
    return this.httpClient.get<any>(url);
  }
}
