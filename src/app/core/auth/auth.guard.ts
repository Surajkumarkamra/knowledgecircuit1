import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { CanActivate } from '../../../../node_modules/@angular/router/src/utils/preactivation';
import { AuthService } from '../../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard {
  path: ActivatedRouteSnapshot[];
  route: ActivatedRouteSnapshot;
  constructor(
    private router: Router,
    private authService: AuthService
) { }

canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
  // console.log(state);
    // const currentUser = this.authService.currentUserValue;
    // if (true) {
    //     // logged in so return true
    //     this.router.navigate(['/dashboard']);
    //     return true;
    // }

    // not logged in so redirect to login page with the return url
    // this.router.navigate(['/dashboard']);
    // return false;
}
}
