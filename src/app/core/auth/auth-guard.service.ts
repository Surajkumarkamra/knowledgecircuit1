import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { selectorAuth } from './auth.reducer';

@Injectable()
export class AuthGuardService implements CanActivate {
  isAuthenticated = false;

  constructor(private store: Store<any>,
    private router: Router) {
    this.store
      .select(selectorAuth)
      .subscribe(auth => (this.isAuthenticated = auth.isAuthenticated));
  }
  canActivate(): boolean {
    if (!this.isAuthenticated) { this.router.navigate(['/login']); }
    return this.isAuthenticated;
  }
}
