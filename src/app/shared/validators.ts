import { AbstractControl, ValidatorFn } from '@angular/forms';
import { Subscription } from 'rxjs';
import { FormControl } from '@angular/forms';

export function matchOtherValidator(otherControlName: string): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    const otherControl: AbstractControl = control.root.get(otherControlName);

    if (otherControl) {
      const subscription: Subscription = otherControl.valueChanges.subscribe(() => {
        control.updateValueAndValidity();
        subscription.unsubscribe();
      }
      );
    }
    return (otherControl && control.value !== otherControl.value) ? { match: true } : null;
  };
}

export function emailValidator(control: FormControl): { [key: string]: any } {
  // tslint:disable-next-line:max-line-length
  const emailRegexp = /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/i;
  if (control.value && control.value.trim() && !emailRegexp.test(control.value)) {
    return { invalidEmail: true };
  }
}


