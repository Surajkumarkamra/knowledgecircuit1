import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IImage } from 'ng-simple-slideshow';
import { AuthService } from '../services';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  imageUrls: (string | IImage)[] = [
    { url: '../../../assets/slides/slide-1/1.jpg' },
    { url: '../../../assets/slides/slide-1/2.jpg'},
    { url: '../../../assets/slides/slide-1/3.jpg'},
    { url: '../../../assets/slides/slide-1/4.jpg'},
    { url: '../../../assets/slides/slide-1/5.jpg'},
  ];

  imageUrls1: (string | IImage)[] = [
    { url: '../../../assets/slides/slide-2/1.jpg' },
    { url: '../../../assets/slides/slide-2/2.jpg'},
    { url: '../../../assets/slides/slide-2/3.jpg'},
    { url: '../../../assets/slides/slide-2/4.jpg'},
    { url: '../../../assets/slides/slide-2/5.jpg'},
  ];
  height = '400px';
  minHeight;
  showArrows = false;
  disableSwiping = true;
  autoPlay = true;
  autoPlayInterval = 3333;
  debug = false;
  backgroundSize = 'cover';
  backgroundPosition = 'center center';
  backgroundRepeat = 'no-repeat';
  showDots = true;
  dotColor = '#FFF';
  showCaptions = true;
  captionColor = '#FFF';
  captionBackground = 'rgba(0, 0, 0, .35)';
  lazyLoad = false;
  hideOnNoSlides = false;
  width = '100%';
  fullscreen = false;

  constructor(private router: Router,
    private authSevice: AuthService) { }

  ngOnInit() {
    if (this.authSevice.getUser().id) { this.router.navigate(['dashboard']); }
  }

  goToAboutUs() {
    this.router.navigate(['about']);
  }

}
