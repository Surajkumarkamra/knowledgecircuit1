import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { MainScreenModule } from '../main-screen';

import { EducatorComponent } from './educator/educator.component';


const educatorRoutes: Routes = [{
  path: '',
  component: EducatorComponent,
  data: { title: 'Educator' },
}];


@NgModule({
  imports: [
    CommonModule,
    MainScreenModule,
    RouterModule.forChild(educatorRoutes)
  ],
  exports: [RouterModule],
  declarations: [EducatorComponent]
})
export class EducatorModule { }
